## Bookmarks

### linux

[Linux man pages](https://linux.die.net/man/)

[InitRAMFS, Dracut, and the Dracut Emergency Shell - FedoraMagazine](https://fedoramagazine.org/initramfs-dracut-and-the-dracut-emergency-shell/)

[Open vSwitch Documentation --- Open vSwitch 2.13.90documentation](http://docs.openvswitch.org/en/latest/)

[Playing with Bonding on OpenvSwitch](https://brezular.com/2011/12/04/openvswitch-playing-with-bonding-on-openvswitch/)

[netfilter/iptables project homepage - The netfilter.orgproject](https://netfilter.org/)

[nftableswiki](https://wiki.nftables.org/wiki-nftables/index.php/Main_Page)

[Keepalived for Linux](https://keepalived.org/manpage.html)

[HowTos/SELinux](https://wiki.centos.org/HowTos/SELinux#head-02c04b0b030dd3c3d58bb7acbbcff033505dd3af)

[conntrackd.conf(5) --- conntrackd --- Debian testing --- Debian Manpages](https://manpages.debian.org/testing/conntrackd/conntrackd.conf.5.en.html)

[nginx documentation](https://nginx.org/en/docs/)

[Documentation Project - The Apache HTTP Server Project](https://httpd.apache.org/docs-project/)

[Gentoo Wiki](https://wiki.gentoo.org/wiki/Main_Page)

[ArchWiki](https://wiki.archlinux.org/)

[GDB Documentation](https://www.gnu.org/software/gdb/documentation/)

[Red Hat Ansible Automation Platform](https://www.ansible.com/products/automation-platform)

[Securing Linux with a Faster and Scalable Iptables - 21-Securing\_Linux\_with\_a\_Faster\_and\_Scalable\_Iptables.pdf](https://sebymiano.github.io/documents/21-Securing_Linux_with_a_Faster_and_Scalable_Iptables.pdf)

[Getting Started - Vagrant by HashiCorp](https://www.vagrantup.com/intro/getting-started/)

[Docker Documentation \| Docker Documentation](https://docs.docker.com/)

[Docker Engine overview \| Docker Documentation](https://docs.docker.com/engine/)

[Kubernetes Documentation - Kubernetes](https://kubernetes.io/docs/home/)

[libvirt:](https://libvirt.org/docs.html)

[Documentation - QEMU](https://wiki.qemu.org/Documentation)

[freeradius Documentation](https://freeradius.org/documentation/)

[Gitea - Docs](https://docs.gitea.io/en-us/)

[RRDtool - About RRDtool](https://oss.oetiker.ch/rrdtool/)

[ModSecurity: Open Source Web Application
Firewall](https://www.modsecurity.org/)

[Docs \| Suricata](https://suricata-ids.org/docs/)

[Snort - Network Intrusion Detection & Prevention
System](https://www.snort.org/)

[squid : Optimising Web Delivery](http://www.squid-cache.org/)

[GCC online documentation - GNU Project - Free Software Foundation
(FSF)](https://gcc.gnu.org/onlinedocs/)

[GPU passthrough: gaming on Windows on
Linux](https://davidyat.es/2016/09/08/gpu-passthrough/)

[tcpdump examples](https://danielmiessler.com/study/tcpdump/)

### standards

[Filesystem Hierarchy Standard](https://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html)

[LSB Specifications](https://refspecs.linuxfoundation.org/lsb.shtml)

[Active IETF working groups](https://datatracker.ietf.org/wg/)

[Locator/ID Separation Protocol (lisp) - Documents](https://datatracker.ietf.org/wg/lisp/documents/)

[RFC INDEX](https://www.rfc-editor.org/rfc-index.html)

[Internet Assigned Numbers Authority](https://www.iana.org/)

[POSIX: The Open Group Base Specifications Issue 7, 2018 edition](https://pubs.opengroup.org/onlinepubs/9699919799/)

### networks

[OSPF Design Guide - Cisco](https://www.cisco.com/c/en/us/support/docs/ip/open-shortest-path-first-ospf/7039-1.html)

[IP Routing: BGP Configuration Guide - Cisco](https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/iproute_bgp/configuration/xe-16/irg-xe-16-book.html)

[Configure VXLAN - Cisco](https://www.cisco.com/c/en/us/support/docs/switches/nexus-9000-series-switches/118978-config-vxlan-00.html)

[Best Practices for Virtual Port Channels (vPC) on Cisco Nexus 7000 Series Switches - vpc\_best\_practices\_design\_guide.pdf](https://www.cisco.com/c/dam/en/us/td/docs/switches/datacenter/sw/design/vpc_design/vpc_best_practices_design_guide.pdf)

[Understanding vPC Election Process - Cisco](https://www.cisco.com/c/en/us/support/docs/ios-nx-os-software/nx-os-software/212589-understanding-vpc-election-process.html)

[Cisco IOS Multiprotocol Label Switching Configuration Guide, Release 12.2SR - Cisco](https://www.cisco.com/c/en/us/td/docs/ios/mpls/configuration/guide/12_2sr/mp_12_2sr_book.html)

[MPLS Layer 2 VPNs Configuration Guide, Cisco IOS XE Release 3S - Configuring Virtual Private LAN Services - Cisco](https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/mp_l2_vpns/configuration/xe-3s/mp-l2-vpns-xe-3s-book/mp-vpls.html)

[Configuring LISP - Cisco](https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/iproute_lisp/configuration/xe-3s/irl-xe-3s-book/irl-cfg-lisp.html)

[Article: K05939436 - BIG-IP TMOS operations guide \| Chapter 1: Guide
introduction and contents](https://support.f5.com/csp/article/K05939436)

[Deployment Guides F5](https://www.f5.com/services/resources/deployment-guides)

[SteelHead CX](https://support.riverbed.com/content/support/software/steelhead/cx-appliance.html)

[Ankenbrand - Checkpoint R80](https://www.ankenbrand24.de/index.php/articles/articles-check-point/)

### python

[PEP 0 \-- Index of Python Enhancement Proposals (PEPs) Python.org](https://www.python.org/dev/peps/)

[CheckPointSW/cp\_mgmt\_api\_python\_sdk: Check Point API Python Development Kit](https://github.com/CheckPointSW/cp_mgmt_api_python_sdk)

[ktbyers/netmiko: Multi-vendor library to simplify Paramiko SSHconnections to network devices](https://github.com/ktbyers/netmiko)

[git-hub: napalm-automation](https://github.com/napalm-automation)

[Napalm Network Automation](https://napalm-automation.net/)

[Welcome to Flask --- Flask Documentation (1.1.x)](https://flask.palletsprojects.com/en/1.1.x/)

[Jinja --- Jinja Documentation (2.11.x)](https://jinja.palletsprojects.com/en/2.11.x/)

[Welcome to the tox automation project --- tox 3.14.7.dev7 documentation](https://tox.readthedocs.io/en/latest/)

[pytest: helps you write better programs --- pytest documentation](https://docs.pytest.org/en/latest/)

[26.3. unittest --- Unit testing framework --- Python 3.4.10 documentation](https://docs.python.org/3.4/library/unittest.html)

[Module Index --- Ansible Documentation](https://docs.ansible.com/ansible/latest/modules/modules_by_category.html)

[The Python Profilers --- Python 3.7 documentation](https://docs.python.org/3/library/profile.html#module-cProfile)

[PythonSpeed/PerformanceTips - Python](https://wiki.python.org/moin/PythonSpeed/PerformanceTips)

[The Python Profilers --- Python 3.7 documentation](https://docs.python.org/3.7/library/profile.html)

[pdb --- The Python Debugger --- Python 3.7 documentation](https://docs.python.org/3/library/pdb.html)

### golang

[Concurrency --- An Introduction to Programming in Go](https://www.golang-book.com/books/intro/10)

[Arrays, Slices and Maps --- An Introduction to Programming in Go](https://www.golang-book.com/books/intro/6)

[Defer, Panic & Recover -\> Functions --- An Introduction to Programming in Go](https://www.golang-book.com/books/intro/7#section6)

[Structs and Interfaces --- An Introduction to Programming in Go ](https://www.golang-book.com/books/intro/9#section3)

[Testing --- An Introduction to Programming in Go](https://www.golang-book.com/books/intro/12)

[The Core Packages --- An Introduction to Programming in Go](https://www.golang-book.com/books/intro/13#section6)

[bytes - The Go Programming Language](https://golang.org/pkg/bytes/)

[strings - The Go Programming Language](https://golang.org/pkg/strings/)

[Packages - The Go Programming Language](https://golang.org/pkg/)

### perl

[The Comprehensive Perl Archive Network](https://www.cpan.org/)

[Raku Programming Language](https://raku.org/)

[Documentation - Rakudo Compiler for Raku Programming Language](https://rakudo.org/docs)

[Raku Guide](https://raku.guide/)

[Library - www.perl.org](https://www.perl.org/books/library.html)

# c

[ebook - The C Programming Language Ritchie & kernighan -.doc - The C-Programming Language.pdf](http://www2.cs.uregina.ca/~hilder/cs833/Other%20Reference%20Materials/The%20C%20Programming%20Language.pdf)

[C Library Overview](https://www.mers.byu.edu/docs/standardC/_index.html)

[Debugging with GDB](https://sourceware.org/gdb/current/onlinedocs/gdb/)

[C++ International Standard -n3337.pdf](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2012/n3337.pdf)

[ISO/IEC 9899:1999 -n1256.pdf](http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf)

[Welcome to Clang\'s documentation! --- Clang 11 documentation](https://clang.llvm.org/docs/index.html)

[Clang Compiler User's Manual --- Clang 11 documentation](https://clang.llvm.org/docs/UsersManual.html)

[ARM Options (Using the GNU Compiler Collection(GCC))](https://gcc.gnu.org/onlinedocs/gcc/ARM-Options.html)

[x86 Options (Using the GNU Compiler Collection(GCC))](https://gcc.gnu.org/onlinedocs/gcc/x86-Options.html)

[Optimize Options (Using the GNU Compiler Collection(GCC))](https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html)

[Intel® 64 and IA-32 Architectures Software Developer Manuals](https://software.intel.com/en-us/articles/intel-sdm)

[Jenkins User Documentation](https://jenkins.io/doc/)

# misc

[JSch - Java Secure Channel](http://www.jcraft.com/jsch/)

[debug bigfix](https://www.ibm.com/support/knowledgecenter/en/SS8JFY_9.2.0/com.ibm.lmt.doc/Inventory/probdet/c_pd_main.html)
